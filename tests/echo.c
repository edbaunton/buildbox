#include <stdio.h>

int main(int argc, const char **argv)
{
	for (int i = 1; i < argc; i++) {
		if (i > 1) {
			putchar(' ');
		}
		fputs(argv[i], stdout);
	}
	putchar('\n');

	return 0;
}
