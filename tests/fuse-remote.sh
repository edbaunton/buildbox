#!/bin/bash

. $(dirname "$0")/fuse.sh
. $(dirname "$0")/fuse-basic.sh

if [ -z "$CAS_SERVER" ] ; then
	echo "Missing bst-artifact-server path" >&2
	exit 1
fi

if [ "$#" -lt 1 ] ; then
	echo "Missing test name" >&2
	exit 1
fi
TEST="$1"
shift

mkdir "$TMPDIR/cas-remote"

remote_cleanup() {
	stop_cas_server
	cleanup
}
trap remote_cleanup INT TERM EXIT

start_cas_server() {
	"$CAS_SERVER" -p 0 --enable-push "$TMPDIR/cas-remote" &
	CAS_SERVER_PID=$!
	PORT=
	while [ -z $PORT ] ; do
		sleep 0.1
		[ -e /proc/$CAS_SERVER_PID ]
		PORT=$(netstat -lntp 2>/dev/null | grep "LISTEN.* $CAS_SERVER_PID" | sed -e 's/.*:\([0-9]\+\).*/\1/')
	done
	REMOTE_ARGS="--remote=http://localhost:$PORT"
	if [ -n "$PREFETCH" ] ; then
		REMOTE_ARGS="$REMOTE_ARGS --prefetch"
	fi
}

stop_cas_server() {
	if [ -n "$CAS_SERVER_PID" ] ; then
		kill $CAS_SERVER_PID
		CAS_SERVER_PID=
	fi
}

REF="$TMPDIR/reference"
mkdir "$REF"

# Set up the reference directory
base "$REF"
$TEST "$REF"

start_cas_server

# Create the base via FUSE
start_buildbox
base "$ROOT"

# Apply the test-specific changes via FUSE after restart
restart_buildbox
$TEST "$ROOT"

# Verify that the FUSE root matches the reference directory
compare_trees "$REF" "$ROOT"

# Verify that the FUSE root matches the reference directory
# after restart (save changes in CAS and reload them)
restart_buildbox
compare_trees "$REF" "$ROOT"

stop_buildbox

stop_cas_server
