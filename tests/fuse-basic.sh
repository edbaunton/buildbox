base() {
	mkdir "$1/dir"
	echo "hello, world" > "$1/dir/file"
}

unmodified() {
	true
}

create_empty_file() {
	touch "$1/dir/newfile"
}

create_file() {
	echo "lorem ipsum" > "$1/dir/newfile"
}

create_executable_file() {
	echo "#!/bin/true" > "$1/dir/executable"
	chmod a+x "$1/dir/executable"
}

append_file() {
	echo "lorem ipsum" >> "$1/dir/file"
}

truncate_file() {
	truncate --size=4 "$1/dir/file"
}

fallocate_file() {
	fallocate -o 5 -l 42 "$1/dir/file"
}

replace_file() {
	echo "lorem ipsum" > "$1/dir/newfile"
	mv "$1/dir/newfile" "$1/dir/file"
}

remove_file() {
	rm "$1/dir/file"
}

create_empty_directory() {
	mkdir "$1/dir/subdir"
}

create_directory_with_file() {
	mkdir "$1/dir/subdir"
	echo "lorem ipsum" > "$1/dir/subdir/newfile"
}

remove_directory() {
	rm -r "$1/dir"
}

create_symlink() {
	ln -s "file" "$1/dir/symlink"
}

create_hardlink() {
	ln "$1/dir/file" "$1/dir/hardlink"
}

fifo() {
	mkfifo "$1/dir/fifo"
	echo "lorem ipsum" > "$1/dir/fifo" &
	read line < "$1/dir/fifo"
	[ "$line" = "lorem ipsum" ]
	rm "$1/dir/fifo"
}
